<!--
  -- List of trademarks
  --
  -- Written in 2021 by Márcio Silva <coadde@hyperbola.info>
  --
  -- To the extent possible under law, the author(s) have dedicated all copyright
  -- and related and neighboring rights to this software to the public domain
  -- worldwide. This software is distributed without any warranty.
  --
  -- You should have received a copy of the CC0 Public Domain Dedication along
  -- with this software. If not, see
  -- <https://creativecommons.org/publicdomain/zero/1.0/>.
  -->

__Trademarks:__

*   __[GNU&reg;][GNU]__ and
    *[Free Software Foundation&reg;][FSF_LRT] [(FSF&reg;)][FSF_SRT]*<br/>
    is a registered trademark of _[Free Software Foundation&reg;][FSF]_.
*   __[Freedo&trade;][FREEDO]__
    is a unregistered trademark of _[Rubén Rodrígues Pérez][FREEDO]_.

---

License:
--------

List of trademarks

Written in 2021 by [M&aacute;rcio Silva][COADDE] <coadde@hyperbola.info>

To the extent possible under law,
the author(s) have dedicated all copyright<br/>
and related and neighboring rights to this software to the public domain<br/>
worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along<br/>
with this software. If not, see<br/>
<https://creativecommons.org/publicdomain/zero/1.0/>.

<!-- End of file -->

<!-- Source and trademark websites for GNU® character drawing -->
[GNU]: https://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=85380218
    "GNU® trademark"
[FSF_LRT]: https://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=87909237
    "Free Software Foundation® trademark"
[FSF_SRT]: https://tarr.uspto.gov/servlet/tarr?regser=serial&amp;entry=87909272
    "FSF® trademark"

<!-- Source websites for Freedo™ character drawing -->
[FREEDO]: https://www.fsfla.org/ikiwiki/selibre/linux-libre/#artwork
    "Rubén Rodrígues Pérez"

<!-- License sources -->
[CC]: https://creativecommons.org/
    "Creative Commons®"
[FSF]: https://www.fsf.org/
    "Free Software Foundation®"

<!-- Attributions are applied in this file -->
[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

<!-- Licenses are applied in this file -->
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
    "Creative Commons® Zero 1.0 Public Domain Dedication"
