<!--
  -- Chronologically changes in GNU® and Freedo™ character drawings
  --
  -- Written in 2013-2021 by Márcio Silva <coadde@hyperbola.info>
  --
  -- To the extent possible under law, the author(s) have dedicated all copyright
  -- and related and neighboring rights to this software to the public domain
  -- worldwide. This software is distributed without any warranty.
  --
  -- You should have received a copy of the CC0 Public Domain Dedication along
  -- with this software. If not, see
  -- <https://creativecommons.org/publicdomain/zero/1.0/>.
  -->

### ___Version:___ 1.0.1 [2021-05-19]

*   _Update Markdown files._

### ___Version:___ 1.0.0 [2021-05-16]

*   _Add all files in version control software._
*   _Update metadata files._
*   _Update _**README (and readme)**_ to _
**README.md (and COPYING.md)**_ files._
*   _Update _**AUTHORS**_ to _**ATTRIBUTIONS.md**_ file._
*   _Add _**TRADEMARK.md**_ file._
*   _Rename _**COPYING.fal**_ to _**COPYING_FAL_V1_3**_ file._
*   _Rename _**COPYING.cc**_ to _**COPYING_CCBYSA_V4_0**_ file._
*   _Rename _**COPYING.fdl**_ to _**COPYING_FDL_V1_3**_ file._
*   _Rename _**COPYING.gpl**_ to _**COPYING_GPL_V2_0**_ file._

### ___Version:___ 0.1.4 (1.4; first published version) [2014-03-04]

*   _Add _**AUTHORS**_ and _**readme**_ files
*   _Add _**COPYING.fal**_ file._
*   _Add _**COPYING.cc**_ file._
*   _Add _**COPYING.fdl**_ file._
*   _Add _**COPYING.gpl**_ file._
*   _Remove _**COPYING**_ file._

### ___Version:___ 0.1.3 (1.3)

*   _Add multiple licenses for software and documentation compatibility _
**(CC-BY-SAv4.0+, FALv1.3+, FDLv1.3+ and GPLv2.0+)**
*   _Add _**GNU/Hurd**_ text2 version on _
**GNU Head**_ and _**GNU**_ variant SVG files._
*   _Add _**Freedo**_ text3 version on _
**Freedo Head**_ and _**Freedo**_ variant SVG files._
*   _Add _**St. GNU Head**_ and _**St. GNU**_ variant SVG files._
*   _Fix titles on: _**GNU Head Logo (Black on White Color)**_ and _
**GNU Head and Freedo Logo (Black on White Color)**

### ___Version:___ 0.1.1 (1.1)

*   _Add new _**ChangeLog**_, _**README**_ and _**COPYING**_ files._
*   _Remove transparent color on _**body_color9**_ object to _**GNU**_ and _
**GNU + Freedo**_ (grayscale and color) SVG files._
*   _Rename _**Black on White Color**_ to _**Black on White Color**_ text._
*   _Rename _**White on Black Color**_ to _**White on Black Color**_ text._
*   _Rename _**Line Color**_ to _**Black Line Color**_ text._
*   _Update all SVG files._
*   _Add _**Freedo Head**_ and _**GNU + Freedo Head**_ variant SVG files._
*   _Add _**RGBI 4bit CGA**_ (or _**16 colors CGA**_) version on _
**GNU**_, _**GNU Head**_, _**Freedo**_, _**GNU + Freedo**_ and _
**GNU Head + Freedo**_ variant SVG files._
*   _Add _**White on Black Color**_ and _**White Line Color**_ version on _
**GNU**_, _**GNU Head**_, _**Freedo**_, _**GNU + Freedo**_ and _
**GNU Head + Freedo**_ variant SVG files._

### ___Version:___ 0.1.0 (1.0)

*   _Initial version._

---

License:
--------

Chronologically changes in GNU&reg; and Freedo&trade; character drawings

Written in 2013-2021 by [M&aacute;rcio Silva][COADDE] <coadde@hyperbola.info>

To the extent possible under law,
the author(s) have dedicated all copyright<br/>
and related and neighboring rights to this software to the public domain<br/>
worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along<br/>
with this software. If not, see<br/>
<https://creativecommons.org/publicdomain/zero/1.0/>.

<!-- End of file -->

<!-- License sources -->
[CC]: https://creativecommons.org/
    "Creative Commons®"

<!-- Attributions are applied in this file -->
[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

<!-- Licenses are applied in this file -->
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
    "Creative Commons® Zero 1.0 Public Domain Dedication"
