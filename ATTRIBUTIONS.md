<!--
  -- List of attributions
  --
  -- Written in 2021 by Márcio Silva <coadde@hyperbola.info>
  --
  -- To the extent possible under law, the author(s) have dedicated all copyright
  -- and related and neighboring rights to this software to the public domain
  -- worldwide. This software is distributed without any warranty.
  --
  -- You should have received a copy of the CC0 Public Domain Dedication along
  -- with this software. If not, see
  -- <https://creativecommons.org/publicdomain/zero/1.0/>.
  -->

__Attributions:__

* ___[Jos&eacute; Silva (Crazytoon) &lt;crazytoon@hyperbola.info&gt;][CRAZYTOON]___

      Original author of drawings, most clean-ups and
      GNU&reg; colours (2013-2014)

* ___[M&aacute;rcio Silva (Co. Ad. De.) &lt;coadde@hyperbola.info&gt;][COADDE]___

      Digitalizations, clean-ups and colouring (2013-2014)<br/>
      Freedo&trade; colours, based of original Freedo&trade; (2013-2014)

---

License:
--------

List of attributions

Written in 2021 by [M&aacute;rcio Silva][COADDE] <coadde@hyperbola.info>

To the extent possible under law,
the author(s) have dedicated all copyright<br/>
and related and neighboring rights to this software to the public domain<br/>
worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along<br/>
with this software. If not, see<br/>
<https://creativecommons.org/publicdomain/zero/1.0/>.

<!-- End of file -->

<!-- License sources -->
[CC]: https://creativecommons.org/
    "Creative Commons®"

<!-- List of attributions -->
[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <crazytoon@hyperbola.info>"
[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

<!-- Licenses are applied in this file -->
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
    "Creative Commons® Zero 1.0 Public Domain Dedication"
