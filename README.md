<!--
  -- Read me and Copyright file
  --
  -- Written in 2021 by Márcio Silva <coadde@hyperbola.info>
  --
  -- To the extent possible under law, the author(s) have dedicated all copyright
  -- and related and neighboring rights to this software to the public domain
  -- worldwide. This software is distributed without any warranty.
  --
  -- You should have received a copy of the CC0 Public Domain Dedication along
  -- with this software. If not, see
  -- <https://creativecommons.org/publicdomain/zero/1.0/>.
  -->

<img alt="GNU+FREEDO" src="gnu+freedo-logo.svg" text="GNU&reg; and Freedo&trade; character drawings" height="256">
<img alt="GNU+FREEDO_HEAD" src="gnu+freedo_head-logo.svg" text="GNU® and Freedo™ character head drawings" height="128">

---

__[GNU&reg; and Freedo&trade; charater drawings][GNU_SRC]__
===========================================================

The _trademarks_ are included in:

*   The __[TRADEMARKS.md][TRADEMARKS]__ file which contains
    _all trademarks_ for this _source_.
*   _Inside_ each _file_.

The _attributions_ are included in:

*   The __[ATTRIBUTIONS.md][ATTRIBUTIONS]__ file which contains
    _all attributions_ for this _source_.
*   _Inside_ each _file_.

The _changelog_ are included in:

*   The __[CHANGELOG.md][CHANGELOG]__ file which contains
    _all changes_ for this _source_.

---

In all content files;<br/>
you can _redistribute it_ and/or _modify it_ under the _terms of either_:

1.  The ___[Free Art License][FAL]___<br/>
    as published by ___Copyleft Attitude___;<br/>
    either _version_ ___1.3___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_FAL_V1_3][FAL]***_)_.

2.  The
    ___[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]___<br/>
    as published by ___[Creative Commons&reg;][CC]___;<br/>
    either _version_ ___4.0___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_CCBYSA_V4_0][CCBYSA]***_)_.

3.  The ___[GNU&reg; Free Documentation License][FDL]___<br/>
    as published by the ___[Free Software Foundation&reg;][FSF]___;<br/>
    either _version_ ___1.3___, or _(at your option)_ any _later_ version<br/>
    with _no Invariant Sections_,<br/>
    _no Front-Cover Texts_, and _no Back-Cover Texts_<br/>
    _(read_ ***[COPYING_FDL_V1_3][FDL]***_)_.

4.  The ___[GNU&reg; General Public License][GPL]___<br/>
    as published by the ___[Free Software Foundation&reg;][FSF]___;<br/>
    either _version_ ___2.0___, or _(at your option)_ any _later_ version<br/>
    _(read_ ***[COPYING_GPL_V2_0][GPL]***_)_.

---

Licenses of _drawings_:
-----------------------

*   [__GNU&reg;__ and __Freedo&trade;__ character drawings _2013/12/27_]
[GNU+FREEDO_HEAD]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_ and _[M&aacute;rcio Silva][COADDE]_
    is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_,<br/>
    _[GNU&reg; Free Documentation License version 1.3][FDL]_<br/>
    with _no Invariant Sections_,</br>
    _no Front-Cover Texts_, and _no Back-Cover Texts_ or<br/>
    _[GNU&reg; General Public License version 2.0][GPL]_.

*   [__GNU&reg;__ and __Freedo&trade;__ character head drawings _2014/01/27_]
[GNU+FREEDO_HEAD]<br/>
    by _[Jos&eacute; Silva][CRAZYTOON]_ and _[M&aacute;rcio Silva][COADDE]_
    is under the terms of either:<br/>
    _[Free Art License version 1.3][FAL]_,<br/>
    _[Creative Commons&reg; Attribution-ShareAlike 4.0 International License]
[CCBYSA]_,<br/>
    _[GNU&reg; Free Documentation License version 1.3][FDL]_<br/>
    with _no Invariant Sections_,<br/>
    _no Front-Cover Texts_, and _no Back-Cover Texts_ or<br/>
    _[GNU&reg; General Public License version 2.0][GPL]_.

---

License:
--------

Read me and Copyright file

Written in 2021 by [M&aacute;rcio Silva][COADDE] <coadde@hyperbola.info>

To the extent possible under law,
the author(s) have dedicated all copyright<br/>
and related and neighboring rights to this software to the public domain<br/>
worldwide. This software is distributed without any warranty.

You should have received a copy of the CC0 Public Domain Dedication along<br/>
with this software. If not, see<br/>
<https://creativecommons.org/publicdomain/zero/1.0/>.

<!-- End of file -->

<!-- Original source files -->
[GNU_SRC]: https://www.gnu.org/graphics/gnu-and-freedo/gnu-and-freedo.html
    "GNU® and Freedo™ charater drawings in The GNU® Art Gallery"

<!-- Drawing files -->
[GNU+FREEDO]: gnu+freedo-logo.svg
    "GNU® and Freedo™ character drawings"

[GNU+FREEDO_HEAD]: gnu+freedo_head-logo.svg
    "GNU® and Freedo™ head character drawings"

<!-- TRADEMARKS.md, ATTRIBUTIONS.md and CHANGELOG.md files -->
[TRADEMARKS]: TRADEMARKS.md
    "GNU® and Freedo™ Trademarks"
[ATTRIBUTIONS]: ATTRIBUTIONS.md
    "GNU® and Freedo™ Attributions"
[CHANGELOG]: CHANGELOG.md
    "Changelog"

<!-- License sources -->
[CC]: https://creativecommons.org/
    "Creative Commons®"
[FSF]: https://www.fsf.org/
    "Free Software Foundation®"

<!-- List of attributions -->
[CRAZYTOON]: crazytoon@hyperbola.info
    "José Silva (Crazytoon) <crazytoon@hyperbola.info>"
[COADDE]: coadde@hyperbola.info
    "Márcio Silva (Co.Ad.De.) <coadde@hyperbola.info>"

<!-- Licenses are applied in this file -->
[CC0]: https://creativecommons.org/publicdomain/zero/1.0/
    "Creative Commons® Zero 1.0 Public Domain Dedication"

<!-- Licenses are applied in drawing files -->
[FAL]: https://artlibre.org/licence/lal/en/
    "Free Art License version 1.3"
[CCBYSA]: https://creativecommons.org/licenses/by-sa/4.0/
    "Creative Commons® Attribution-ShareAlike 4.0 International License"
[FDL]: https://www.gnu.org/licenses/fdl-1.3
    "GNU® Free Documentation License version 1.3"
[GPL]: https://www.gnu.org/licenses/gpl-2.0
    "GNU® General Public License version 2.0"
